import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './vista/login/login.component';
import { DashboardComponent } from './vista/dashboard/dashboard.component';
import { InstitucionesComponent } from './vista/instituciones/instituciones.component';
import { SedesComponent } from './vista/sedes/sedes.component';
import { GruposComponent } from './vista/grupos/grupos.component';
import { InfogGrupoComponent } from './vista/infogrupo/infog-grupo.component';

const routes: Routes = [
  {path:'', redirectTo:'login' , pathMatch: 'full'},
  { path:'login' , component:LoginComponent},
  { path:'dashboard' , component:DashboardComponent},
  { path:'instituciones' , component:InstitucionesComponent},
  { path:'sedes' , component:SedesComponent},
  { path:'grupos' , component:GruposComponent},
  { path: 'infogrupos', component:InfogGrupoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routinComponents = [LoginComponent, DashboardComponent, InstitucionesComponent, SedesComponent, GruposComponent, InfogGrupoComponent]
