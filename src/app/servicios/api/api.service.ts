import { Injectable } from '@angular/core';
import { Welcome } from 'src/app/modelos/dashboard.interface';
import { ResponseI } from 'src/app/modelos/response.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { institucionesI } from 'src/app/modelos/instituciones.interface';
import { SedeI } from 'src/app/modelos/sedes.interface';
import { GruposI } from 'src/app/modelos/grupos.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  


  constructor(private http:HttpClient) { }

 /*  getAllMunicipiosByLogin(form:LoginI):Observable<ResponseI>{
    let direccion = this.url + "auth";
    return this.http.post<ResponseI>(direccion,form);
  } */
  GetAllMunicipios(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let data= {
      User: 'etraining',
      Password: 'explorandoando2020%',
      option: 'municipios'
     
      }
    
    return this.http.post<Welcome>('https://www.php.engenius.com.co/DatabaseIE.php',data);


  }
  GetAllInstituciones(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let data= {
      User: 'etraining',
      Password: 'explorandoando2020%',
      option: 'instituciones',
      CodMun: '73168'
      }
    
    return this.http.post<institucionesI>('https://www.php.engenius.com.co/DatabaseIE.php',data);


  }

  GetAllSedes(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let data= {
      User: 'etraining',
      Password: 'explorandoando2020%',
      option: 'sedes',
      CodInst: '273168002111'
     
      }
    
    return this.http.post<SedeI>('https://www.php.engenius.com.co/DatabaseIE.php',data);


  }
  GetAllgrupos(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let data= {
      User: 'etraining',
      Password: 'explorandoando2020%',
      option: 'grupos',
      CodSede: '273268001201'

     
      }
    
    return this.http.post<GruposI>('https://www.php.engenius.com.co/DatabaseIE.php',data);


  }

  GetAllGrupoInfo(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    let data= {
      User: 'etraining',
      Password: 'explorandoando2020%',
      option: 'infoGrupo',
      IdGrupo: '10028'
     
      }
    
    return this.http.post('https://www.php.engenius.com.co/DatabaseIE.php',data);


  }




  /*getIntitucionesMunicipios (String CodMun){
    return

  }

  getSedesIntituciones (String CodInst){
    return

  }

  getGruposSede (String IdGrupo){
    return

  }*/
}
