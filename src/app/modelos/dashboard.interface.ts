export interface Welcome {
    login:  string;
    option: string;
    data:   Datum[];
}

export interface Datum {
    nombre: string;
    dane:   string;
}