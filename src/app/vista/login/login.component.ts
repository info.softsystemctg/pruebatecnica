import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { Welcome } from 'src/app/modelos/dashboard.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
  
})
export class LoginComponent implements OnInit {
  loginForm= new FormGroup ({
    User: new FormControl ('', Validators.required),
    Password: new FormControl ('', Validators.required)
  })

  constructor( private api:ApiService) { }

  ngOnInit(): void {
  }
  
  onLogin(form:Welcome){
    
    /*this.api.getAllMunicipiosByLogin(form).subscribe(data =>{
      console.log(data);

  })*/
  }

}
