import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfogGrupoComponent } from './infog-grupo.component';

describe('InfogGrupoComponent', () => {
  let component: InfogGrupoComponent;
  let fixture: ComponentFixture<InfogGrupoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfogGrupoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfogGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
