import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-infog-grupo',
  templateUrl: './infog-grupo.component.html',
  styleUrls: ['./infog-grupo.component.css']
})
export class InfogGrupoComponent implements OnInit {

  constructor(private infoGrupoServices:ApiService) { 
    this.infoGrupoServices.GetAllGrupoInfo().subscribe(resp =>{
      console.log(resp);

    });
  }

  ngOnInit(): void {
  }

}
