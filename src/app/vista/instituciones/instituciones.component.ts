import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-instituciones',
  templateUrl: './instituciones.component.html',
  styleUrls: ['./instituciones.component.css']
})
export class InstitucionesComponent implements OnInit {

  public instituciones: any =[];

  constructor(private institucionesService:ApiService) {
    this.institucionesService.GetAllInstituciones().subscribe(resp =>{
      console.log(resp);
      this.instituciones=resp.data;


    });
   }

  ngOnInit(): void {
  }

}
