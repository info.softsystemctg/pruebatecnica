import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-sedes',
  templateUrl: './sedes.component.html',
  styleUrls: ['./sedes.component.css']
})
export class SedesComponent implements OnInit {
  public sedes: any =[];
  constructor(private sedesServices:ApiService) {
    this.sedesServices.GetAllSedes().subscribe(resp =>{
      console.log(resp);
      this.sedes =resp.data;
    });
  }

  ngOnInit(): void {
  }

}
