import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {
  public grupos: any =[];
  constructor(private grupoServices:ApiService) {
    this.grupoServices.GetAllgrupos().subscribe(resp =>{
      console.log(resp);
      this.grupos=resp.data;

    });
   }

  ngOnInit(): void {
  }

}
