import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api/api.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public municipios: any =[]
  constructor(private municipiosservices:ApiService) { 
    this.municipiosservices.GetAllMunicipios().subscribe(resp =>{

      console.log(resp);
      this.municipios =resp.data;

    });

  }

  ngOnInit(): void {
  }

}
